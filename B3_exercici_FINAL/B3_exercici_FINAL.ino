#include <AFMotor.h>
#include <PID_v1.h>
#include <Event.h>
#include <Timer.h>
#include <Ultrasonic.h>
#include <Bluetooth.h>
#include <Buzzer.h>
#include <Speedometer.h>

// Defined Action Commands
#define COMMAND_LEFT '1'
#define COMMAND_RIGHT '2'
#define COMMAND_FORWARD '3'
#define COMMAND_BACKWARD '4'
#define COMMAND_BUZZER '5'
#define COMMAND_STOP '6'

// Proximity sensors
Ultrasonic distance_rear(40,41);
Ultrasonic distance_front(42,43);

//Speed sensors
Speedometer speed_left(18);
Speedometer speed_right(19);

// Motors
AF_DCMotor motor1(1, MOTOR12_1KHZ);
AF_DCMotor motor2(2, MOTOR12_1KHZ);
AF_DCMotor motor3(3, MOTOR34_1KHZ);
AF_DCMotor motor4(4, MOTOR34_1KHZ);

//Buzzer
Buzzer buzzer(24);

//Bluetooth
Bluetooth bluetooth;

//Timer
Timer t1, t2, t3;

// Received command
char received_command;
int speed;
int frontAvailable = 0;
int rearAvailable = 0;
int ticksLeft,ticksRight;

void setup()
{
    Serial.begin(9600);
  
    // Buzzer
    buzzer.setup();

    // Bluetooth
    bluetooth.setup();

    // Proximity sensors
    distance_rear.setup();
    distance_front.setup();
    
    speed_left.setup();
    speed_right.setup();
    speed = 255;
  
    // Motor Setup
    motor1.setSpeed(speed);
    motor2.setSpeed(speed);
    motor3.setSpeed(speed);
    motor4.setSpeed(speed);
  
    // Timers
    t1.every(250, SAL_DistanceControl, (void*)1);
    t3.every(1000, SAL_SpeedControl, (void*)1);

    ticksLeft = 0; ticksRight =0;
    attachInterrupt(digitalPinToInterrupt(18),countTicksLeft, RISING);
    attachInterrupt(digitalPinToInterrupt(19),countTicksRight, RISING);   
}

void countTicksLeft(){
  ticksLeft++;
}

void countTicksRight(){
  ticksRight++;
}

void loop() 
{
    // put your main code here, to run repeatedly:
    t1.update();
    t3.update();
}

void serialEvent3()
{   
      if (Serial3.available() > 0) {
          received_command =  bluetooth.read();          
          SAL_Comunication(received_command);
      }
}

// ------------------------------------------------
// SAL Layer
// ------------------------------------------------
void SAL_SpeedControl(void* context) {
  float spl = speed_left.mesureSpeed(ticksLeft);
  float spr = speed_right.mesureSpeed(ticksRight);
  ticksLeft = 0; ticksRight = 0;
  float sp = (spl+spr)/2;
  SAL_sendSpeed(sp);
  //Serial.println("Speed = ");
  //Serial.print(sp);
}

void SAL_DistanceControl(void* context)
{
   /*
   Serial.println("Distance Front:");
   Serial.println(distance_front.getDistance());
   
   Serial.println("Distance Rear:");
   Serial.println(distance_rear.getDistance());
   */
   // Check Distance Forward
   if (distance_front.getDistance() < 10)
   {
      frontAvailable = 0;
      if(received_command == COMMAND_FORWARD || received_command == COMMAND_LEFT || received_command == COMMAND_RIGHT)
      {
          SAL_Stop();
      }  
   }
   else
      frontAvailable = 1; // Forward is clear, can continue driving.

   // Check Distance Backwards   
   if (distance_rear.getDistance() < 10)
   {
      rearAvailable = 0;
      if(received_command == COMMAND_BACKWARD || received_command == COMMAND_LEFT || received_command == COMMAND_RIGHT)
      {
          SAL_Stop();
      }
   }
   else
      rearAvailable = 1; // Backward is clear, can continue driving.
}

void SAL_Buzzer()
{
    buzzer.buzz();
}

void SAL_Stop() 
{
    // Stop motors
    motor1.run(RELEASE);
    motor2.run(RELEASE);
    motor3.run(RELEASE);
    motor4.run(RELEASE);
}

void SAL_Forward() 
{
    // Set motors to run forward
    motor1.run(FORWARD);
    motor2.run(FORWARD);
    motor3.run(FORWARD);
    motor4.run(FORWARD);
}

void SAL_Backward() 
{
    // Set motors to run backward
    motor1.run(BACKWARD);
    motor2.run(BACKWARD);
    motor3.run(BACKWARD);
    motor4.run(BACKWARD);
}

void SAL_Left() 
{
    // Set motors to turn left
    motor1.run(BACKWARD);
    motor2.run(FORWARD);
    motor3.run(FORWARD);
    motor4.run(BACKWARD);
}

void SAL_Right() 
{
    // Set motors to turn right
    motor1.run(FORWARD);
    motor2.run(BACKWARD);
    motor3.run(BACKWARD);
    motor4.run(FORWARD);
}

void SAL_Comunication(char command)
{
    switch (command)
    {
       case COMMAND_LEFT:
          SAL_Stop();
          SAL_Left();
          break;
       case COMMAND_RIGHT:
          SAL_Stop();
          SAL_Right();
          break;
       case COMMAND_FORWARD:
          if(frontAvailable){
            SAL_Stop();
            SAL_Forward();
          }
          break;
       case COMMAND_BACKWARD:
          if(rearAvailable){
            SAL_Stop();
            SAL_Backward(); 
          }
          break;
       case COMMAND_BUZZER:
          SAL_Buzzer();
          break;
       case COMMAND_STOP:
          SAL_Stop();
          break;
       default:
          break;         
    }   
}

void SAL_sendSpeed(float sp)
{
    bluetooth.write((char) sp);
}

