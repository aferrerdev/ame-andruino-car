// -----------------------------------------
// Bluetooth Library
// created by Àlex Ferrer and Sergio Pérez

#include <Bluetooth.h>

//constructor
Bluetooth::Bluetooth()
{
	
}

void Bluetooth::setup()
{
	Serial3.begin(9600);

	// To Recieve data from Android.
	pinMode(15,INPUT);
	digitalWrite(15,HIGH);
}

char Bluetooth::read()
{
	return (char) Serial3.read();
}

void Bluetooth::write(int speed)
{
	Serial3.write(speed);
}