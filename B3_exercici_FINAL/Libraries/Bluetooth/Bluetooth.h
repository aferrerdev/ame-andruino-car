// -----------------------------------------
// Bluetooth Library
// created by Àlex Ferrer and Sergio Pérez

#include "Arduino.h"

#define BLUETOOTH_LIBRARY_VERSION 1.0.0

class Bluetooth
{

    public:
        Bluetooth();
        void setup();
       	char read();
        void write(int speed);
};
