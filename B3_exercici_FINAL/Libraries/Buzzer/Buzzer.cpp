// -----------------------------------------
// Buzzer Library
// created by Àlex Ferrer and Sergio Pérez

#include <Buzzer.h>

//constructor
Buzzer::Buzzer(uint8_t _pin)
{
	pin = _pin;
}

void Buzzer::buzz()
{
	analogWrite(pin, 255);
	delay(1000);
	analogWrite(pin, 0);
}

void Buzzer::setup()
{
	pinMode(pin, OUTPUT);
}