// -----------------------------------------
// Buzzer Library
// created by Àlex Ferrer and Sergio Pérez

#include "Arduino.h"

#define BUZZER_LIBRARY_VERSION 1.0.0

class Buzzer
{

    public:
        Buzzer(uint8_t _pin);
        void buzz();
        void setup();

    private:
        uint8_t pin;

};
