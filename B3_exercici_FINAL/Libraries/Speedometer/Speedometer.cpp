// -----------------------------------------
// Speedometer Library
// created by Àlex Ferrer and Sergio Pérez

#include <Speedometer.h>

Speedometer::Speedometer(uint8_t _pin)
{
	pin = _pin;
}

void Speedometer::setup()
{
	pinMode(pin, INPUT);
}

float Speedometer::mesureSpeed(int ticks)
{
	return  ((float)ticks/20.0)*Pi*diameter;
}