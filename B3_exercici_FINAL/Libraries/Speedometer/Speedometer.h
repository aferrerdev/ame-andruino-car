// -----------------------------------------
// Speedometer Library
// created by Àlex Ferrer and Sergio Pérez

#include "Arduino.h"
#include <TimerOne.h>

#define SPEEDOMETER_LIBRARY_VERSION 1.0.0

class Speedometer
{

    public:
        Speedometer(uint8_t _pin);
        void setup();
        float mesureSpeed(int ticks);       

    private:
		uint8_t pin;
		const float Pi = 3.1415926;
		const float diameter = 6.6;

};