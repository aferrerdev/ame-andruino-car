// ------------------------------------------------
// Potentiometer Library
// created by Àlex Ferrer, Sergio Pérez

#include <Ultrasonic.h>

// Constructor
Ultrasonic::Ultrasonic(uint8_t _echoPin, uint8_t _trigPin)
{
	echoPin = _echoPin;
	trigPin = _trigPin;
}

// Inicialitzar pin echo i trigger.
void Ultrasonic::setup()
{
	pinMode(trigPin, OUTPUT);
 	pinMode(echoPin, INPUT);
}

// Obtenir la distancia.
long Ultrasonic::getDistance()
{
	digitalWrite(trigPin, LOW); 
 	delayMicroseconds(2); 

 	digitalWrite(trigPin, HIGH);
 	delayMicroseconds(10); 
 
 	digitalWrite(trigPin, LOW);
 	long duration = pulseIn(echoPin, HIGH);
 
 	//Calculate the distance (in cm) based on the speed of sound.
	long distance = duration/58.2;
	return distance;
}