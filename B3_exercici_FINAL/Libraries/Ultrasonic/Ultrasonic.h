// ------------------------------------------------
// Ultrasonic Library
// created by Àlex Ferrer, Sergio Pérez

#include "Arduino.h"

#define ULTRASONIC_LIBRARY_VERSION 0.1

class Ultrasonic{

	public:
		Ultrasonic(uint8_t echoPin, uint8_t trigPin);
		void setup();
		long getDistance();

	private:
		uint8_t echoPin;
		uint8_t trigPin;
};