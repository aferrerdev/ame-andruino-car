package com.ame.urv.ame_andruino_car;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Singleton object to manage Bluetooth functionalities
 * Created by Alex Ferrer & Sergio Pérez on 6/12/16.
 */
public class Bluetooth {

    private static Bluetooth instance = null;
    private BluetoothAdapter mBluetoothAdapter;

    // Bluetooth Comunication Variables.
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Serial Port Service ID
    private BluetoothDevice device;
    private BluetoothSocket socket;

    private OutputStream outputStream;  // Output to write to arduino.
    private InputStream inputStream;    // Input to read from arduino.

    private boolean deviceConnected = false;
    private boolean stopThread;

    private ControlActivity activity;
    private Thread threadRead;

    /**
     * Create a Bluetooth controller object
     */
    public Bluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Log.e("ERROR", "This device does not support Bluetooth");
        }
    }

    /**
     * Get Bluetooth object instance (Singleton).
     *
     * @return
     */
    public static Bluetooth getInstance() {
        if (instance == null) {
            instance = new Bluetooth();
        }
        return instance;
    }

    /**
     * This function will try to connect the Android Application to the Bluetooth shield of the
     * arduino board.
     *
     * @param device:   Device to connect to.
     * @param activity: Context activity where we are trying to make the connection.
     * @return (true or false) if the connection can be done.
     */
    public boolean connect(BluetoothDevice device, ControlActivity activity) {
        boolean connected = true;

        this.device = device;
        this.activity = activity;

        // Create socket connection.
        try {
            socket = device.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            Log.e("ERROR", e.getMessage());
            connected = false;
        }

        // Get OutputStream
        if (connected) {
            deviceConnected = true;
            try {
                outputStream = socket.getOutputStream();
            } catch (IOException e) {
                Log.e("ERROR", e.getMessage());
            }
            try {
                inputStream = socket.getInputStream();
            } catch (IOException e) {
                Log.e("ERROR", e.getMessage());
            }

        }
        return connected;
    }

    /**
     * Close Connection.
     * This function if the device is connected will finish the reading Thread from Arduino, close
     * the outputStream and the inputStream and also the socket of the connection.
     *
     * @return true or false if it can be done or not.
     */
    public boolean closeConnection() {
        try {
            if (deviceConnected == true) {
                stopReadThread();
                outputStream.close();
                inputStream.close();
                socket.close();
                deviceConnected = false;
                return true;
            }
            return false;
        } catch (IOException e) {
            Log.e("ERROR", e.getMessage());
            return false;
        }
    }

    /**
     * This Thread will run in the background and it has the role of reading all data which the
     * Arduino could be sending to our Android Application.
     */
    public void readThread() {
        final Handler handler = new Handler();
        stopThread = false;
        threadRead = new Thread(new Runnable() {
            public void run() {
                /* While the thread is active and is not stopped, it will read the velocity sended
                    from the arduino in Android. */
                while (!Thread.currentThread().isInterrupted() && !stopThread) {
                    try {
                        int byteCount = inputStream.available();
                        if (byteCount > 0) {
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String string = new String(rawBytes, "UTF-8");
                            handler.post(new Runnable() {
                                public void run() {
                                    activity.setVelocityText(string.getBytes()[0] + " cm/s");
                                    Log.d("Received", "Received data:" + string.getBytes()[0]);
                                }
                            });

                        }
                    } catch (IOException ex) {
                        stopThread = true;
                    }
                }
            }
        });
        threadRead.start();
    }

    /**
     * Function to stop the reading thread if the Thread is still active or alive.
     */
    public void stopReadThread() {
        if (threadRead.isAlive())
            stopThread = true;
    }

    /**
     * Function to send data to Arduino board, we can send a String in a byte array format. This
     * function will write in the outputStream and then will flush the buffer.
     *
     * @param message
     * @throws IOException
     */
    public void sendData(String message) throws IOException {
        outputStream = socket.getOutputStream();
        byte[] command = message.getBytes();
        outputStream.write(command);
        outputStream.flush();
        Log.d("SEND", "Sending Comand:" + message + ", length bytes:" + command.length);
    }

    public BluetoothAdapter getmBluetoothAdapter() {
        return mBluetoothAdapter;
    }

    public void startDiscovery() {
        mBluetoothAdapter.startDiscovery();
    }

    public void cancelDiscovery() {
        mBluetoothAdapter.cancelDiscovery();
    }

    public boolean isDeviceConnected() {
        return deviceConnected;
    }
}