package com.ame.urv.ame_andruino_car;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

/**
 * AME Car by Alex Ferrer & Sergio Pérez
 */
public class ControlActivity extends AppCompatActivity {

    // UIElements
    private TextView deviceAddressLabel;
    private TextView velocityLabel;
    private TextView statusLabel;

    // Bluetooth object
    private Bluetooth bluetooth;

    // Car Commands
    private static int COMMAND_LEFT = 1;
    private static int COMMAND_RIGHT = 2;
    private static int COMMAND_FORWARD = 3;
    private static int COMMAND_BACKWARD = 4;
    private static int COMMAND_BUZZER = 5;
    private static int COMMAND_STOP = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        // Set Title
        setTitle("Arduino Car Controller");

        // Show back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // View Elements
        Button buttonBuzzer = (Button) findViewById(R.id.buttonBuzzer);
        Button buttonStop = (Button) findViewById(R.id.buttonStop);
        Button buttonLeft = (Button) findViewById(R.id.buttonLeft);
        Button buttonRight = (Button) findViewById(R.id.buttonRight);
        Button buttonForward = (Button) findViewById(R.id.buttonForward);
        Button buttonBackward = (Button) findViewById(R.id.buttonBackward);
        deviceAddressLabel = (TextView) findViewById(R.id.deviceAddress);
        velocityLabel = (TextView) findViewById(R.id.velocityLabel);
        statusLabel = (TextView) findViewById(R.id.textView5);

        // Click Listener Forward button
        buttonForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeCommand(COMMAND_FORWARD);
            }
        });


        // Click Listener Backward button
        buttonBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeCommand(COMMAND_BACKWARD);
            }
        });

        // CLick Listener Left button
        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeCommand(COMMAND_LEFT);
            }
        });


        // Click Listener Right button
        buttonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeCommand(COMMAND_RIGHT);
            }
        });

        // Click Listener Stop button: is a click listener because it will stop the vehicle until we
        // touch another action.
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeCommand(COMMAND_STOP);
            }
        });

        // Click Listener Buzzer button: is a click listener because it will play the Buzzer once every
        // time we perfom a click in this button.
        buttonBuzzer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeCommand(COMMAND_BUZZER);
            }
        });

        // Set Bluetooth Device from previous Activity
        if (MainActivity.selectedDevice != null) {
            deviceAddressLabel.setText(MainActivity.selectedDevice.getAddress());
        } else {
            Toast.makeText(ControlActivity.this, "Incorrect device.", Toast.LENGTH_SHORT).show();
            finish();
        }

        // Bluetooth
        bluetooth = Bluetooth.getInstance();

        // Connect Button:
        if (bluetooth.connect(MainActivity.selectedDevice, this)) {
            // Start Reading from arduino using a Thread.
            bluetooth.readThread();
            statusLabel.setText("Connected");
            Toast.makeText(ControlActivity.this, "Connection Opened!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Call sendData of bluetooth object
     *
     * @param command: command to execute in the Arduino Board.
     */
    public void executeCommand(int command) {
        try {
            // We will send data converting the command Integer to String.
            bluetooth.sendData(String.valueOf(command));
        } catch (IOException e) {
            Log.e("ERROR", e.getMessage());
            statusLabel.setText("Disconnected");
            Toast.makeText(ControlActivity.this, "Error with socket, connection closed!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // If device is connected, close Bluetooth socket connection.
        if (bluetooth.isDeviceConnected()) {
            if (bluetooth.closeConnection()) {
                statusLabel.setText("Disconnected");
                Toast.makeText(ControlActivity.this, "Connection closed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Set actual car velocity
     *
     * @param text
     */
    public void setVelocityText(String text) {
        velocityLabel.setText(text);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Action to do when user click on back button
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
