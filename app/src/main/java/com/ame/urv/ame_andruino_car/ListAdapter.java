package com.ame.urv.ame_andruino_car;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Adaptador per a mostrar la llista de dispositius descoberts per el Bluetooth.
 * Created by Alex Ferrer on 6/12/16.
 */
public class ListAdapter  extends BaseAdapter {
    private final Context context;
    private final ArrayList<BluetoothDevice> devices;

    public ListAdapter(Context context, ArrayList<BluetoothDevice> devices) {
        this.context = context;
        this.devices = devices;
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, parent, false);

        TextView name = (TextView) rowView.findViewById(R.id.firstLine);
        TextView mac_adress = (TextView) rowView.findViewById(R.id.secondLine);

        name.setText(devices.get(position).getName());
        mac_adress.setText(devices.get(position).getAddress());

        return rowView;
    }
}