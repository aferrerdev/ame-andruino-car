package com.ame.urv.ame_andruino_car;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

/**
 * AME Car by Alex Ferrer & Sergio Pérez
 */
public class MainActivity extends AppCompatActivity {

    private static int REQUEST_ENABLE_BT = 1;

    // Bluetooth devices list and adapter
    private ArrayList<BluetoothDevice> devices;
    private ListAdapter adapter;

    // UI Elements
    private ListView bluetoothList;
    private Button discoverButton;

    // Bluetooth Components
    private Bluetooth bluetooth;
    public static BluetoothDevice selectedDevice;
    Set<BluetoothDevice> pairedDevices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Arduino Remote Control Car");

        // Bluetooth instance.
        bluetooth = Bluetooth.getInstance();

        // Init devices ArrayList
        devices = new ArrayList<BluetoothDevice>();

        // Elements interfície
        bluetoothList = (ListView) findViewById(R.id.listView);
        discoverButton = (Button) findViewById(R.id.button2);

        // Botó per a descobrir els dispositius bluetooth.
        discoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Si no esta activat activar el bluetooth.
                if (!bluetooth.getmBluetoothAdapter().isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                } else {
                    // Register the BroadcastReceiver, es a dir busquem dispositius bluetooth
                    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy

                    bluetooth.cancelDiscovery();
                    devices.clear();
                    setListViewData(devices);
                    bluetooth.startDiscovery();
                }
            }
        });

        // On Item click listener de la llista de dispositius
        bluetoothList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                // Get Bonded (Paired) Devices.
                pairedDevices = bluetooth.getmBluetoothAdapter().getBondedDevices();

                // Get Bluetooth device from devices list in bluetooth object
                selectedDevice = devices.get(i);

                // Check if the device is Paired
                if (isBounded(selectedDevice)) {
                    // Go to ControllerActivity
                    Intent controlCar = new Intent(MainActivity.this, ControlActivity.class);
                    startActivity(controlCar);
                } else {
                    selectedDevice.createBond();
                    Toast.makeText(MainActivity.this, "You need to pair the devices.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * This function is used to check if the selected device is Paired or not with our Android device.
     *
     * @param device: selected device from list.
     * @return
     */
    private boolean isBounded(BluetoothDevice device) {
        boolean found = false;
        for (BluetoothDevice e : pairedDevices) {
            if (e.equals(device))
                found = true;
        }
        return found;
    }

    // Create a BroadcastReceiver for ACTION_FOUND
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                devices.add(device);
                setListViewData(devices);
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Cancel discovery if app is destroyed.
        bluetooth.cancelDiscovery();
        unregisterReceiver(mReceiver);
    }

    /**
     * Set BluetoothDevice items in the ListView UI widget.
     *
     * @param devices
     */
    void setListViewData(ArrayList<BluetoothDevice> devices) {
        adapter = new ListAdapter(MainActivity.this, devices);
        bluetoothList.setAdapter(adapter);
    }
}
